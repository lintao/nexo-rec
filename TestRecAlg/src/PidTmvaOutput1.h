#ifndef PidTmvaOutput1_h
#define PidTmvaOutput1_h

#include "SniperKernel/AlgBase.h"

class PidTmvaOutput1: public AlgBase 
{
public:
    PidTmvaOutput1(const std::string& name);
    ~PidTmvaOutput1();

    bool initialize();
    bool execute();
    bool finalize();
};

#endif
