#ifndef PidTmvaInput1_h
#define PidTmvaInput1_h

#include "SniperKernel/AlgBase.h"

class PidTmvaInput1: public AlgBase 
{
public:
    PidTmvaInput1(const std::string& name);
    ~PidTmvaInput1();

    bool initialize();
    bool execute();
    bool finalize();
};

#endif
