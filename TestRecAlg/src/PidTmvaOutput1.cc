#include "PidTmvaOutput1.h"
#include "SniperKernel/AlgFactory.h"
#include "SniperKernel/SniperPtr.h"
#include "SniperKernel/SniperDataPtr.h"
#include "BufferMemMgr/EvtDataPtr.h"
#include "EvtNavigator/NavBuffer.h"
#include "BufferMemMgr/IDataMemMgr.h"
#include "Event/PidTmvaHeader.h"

DECLARE_ALGORITHM(PidTmvaOutput1);

PidTmvaOutput1::PidTmvaOutput1(const std::string& name)
    : AlgBase(name)
{

}

PidTmvaOutput1::~PidTmvaOutput1()
{

}

bool
PidTmvaOutput1::initialize()
{
    return true;
}

bool
PidTmvaOutput1::execute()
{
  // SAVE Event Data Model
  // 1. create an event navigator
  nEXO::EvtNavigator* nav = new nEXO::EvtNavigator();
  TTimeStamp ts;
  nav->setTimeStamp(ts);

  // 2. put into data buffer
  SniperPtr<IDataMemMgr> mMgr("BufferMemMgr");
  mMgr->adopt(nav, "/Event");

  // 3. create Header
  nEXO::PidTmvaHeader* header = new nEXO::PidTmvaHeader();
  nav->addHeader(header);

  // 4. create PidTmvaEvent
  nEXO::PidTmvaEvent* event = new nEXO::PidTmvaEvent();
  // * set relationship between header and event
  header->setEvent(event);
  return true;
}

bool
PidTmvaOutput1::finalize()
{
    return true;
}
