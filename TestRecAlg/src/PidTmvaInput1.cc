#include "PidTmvaInput1.h"
#include "SniperKernel/AlgFactory.h"
#include "SniperKernel/SniperPtr.h"
#include "SniperKernel/SniperDataPtr.h"
#include "BufferMemMgr/EvtDataPtr.h"
#include "Event/PidTmvaHeader.h"

DECLARE_ALGORITHM(PidTmvaInput1);

PidTmvaInput1::PidTmvaInput1(const std::string& name)
    : AlgBase(name)
{

}

PidTmvaInput1::~PidTmvaInput1()
{

}

bool
PidTmvaInput1::initialize()
{
    return true;
}

bool
PidTmvaInput1::execute()
{
    EvtDataPtr<nEXO::PidTmvaHeader> header("/Event/PidTmva");
    if (header.invalid()) {
        LogError << "can't find the header." << std::endl;
        return false;
    }
    LogInfo << "dx/dy: "
            << header->get_dx_max() << " "
            << header->get_dy_max()
            << std::endl;
    // update value
    header->set_value(1);
    header->set_value_err(1);
    return true;
}

bool
PidTmvaInput1::finalize()
{
    return true;
}
