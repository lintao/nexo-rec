#ifndef DigiTestAlg_h
#define DigiTestAlg_h

#include "SniperKernel/AlgBase.h"

class DigiTestAlg: public AlgBase 
{
public:
    DigiTestAlg(const std::string& name);
    ~DigiTestAlg();

    bool initialize();
    bool execute();
    bool finalize();
};

#endif
